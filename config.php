<?php

session_start();
ob_start();


define('DB_NAME', 'gsmarena');
define('USER_NAME', 'root');
define('PASSWORD', '');
define('HOST', 'localhost');
define('DEBUG', false);
if (defined('DEBUG') && !DEBUG) {
    error_reporting(0);
}

define('SIGN','&pound');
define('LIBRARIES', 'libs');
define('CLASS_DIR', 'classes');
define('INC_DIR', 'includes');
define('WEBSITE_URL', 'http://localhost/gsmarena/gsmarena/');
define('SITE_URL', WEBSITE_URL);
define('SITE_URL_DASHBOARD', 'http://localhost/gsmarena/gsmarena/administrator/');
define('UPLOADING_PATH', $_SERVER["DOCUMENT_ROOT"]."gsmarena/gsmarena/");


/** Absolute path to the directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', (dirname(__FILE__)));
}
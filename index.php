<?php
include_once 'config.php';
include_once 'classes/core.php';

include_once 'includes/frontend_common/header.php';
?>
<div class="content-container no-padding">
    <div class="container-full">
        <div class="main-content">
            <div class="row row-fluid">
                <div class="col-sm-12">
                    <div class="rev_slider_wrapper fullwidthbanner-container">
                        <div id="rev_slider" class="rev_slider fullwidthabanner">
                            <ul>  
                                <li data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="" data-rotate="0" data-saveperformance="off" data-title="Slide">
                                    <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" width="1920" height="656" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/slider_1920x657.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg">
                                    <div class="tp-caption home1-small-black tp-resizeme" data-x="586" data-y="217" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05">
                                        the best gadgets 2016
                                    </div>

                                    <div class="tp-caption primary-button rev-btn" data-x="628" data-y="342" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bs:solid;bw:1px;cursor:pointer;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power2.easeInOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off">
                                        shop now
                                    </div>

                                    <div class="tp-caption home1-primary tp-resizeme" data-x="427" data-y="261" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05">
                                        changed is everything.
                                    </div>

                                    <div class="tp-caption tp-resizeme" data-x="-391" data-y="-51" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:[-100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="500" data-responsive_offset="on">
                                        <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" width="613" height="707" data-ww="613px" data-hh="707px" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/imac.png">
                                    </div>

                                    <div class="tp-caption tp-resizeme" data-x="172" data-y="311" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power2.easeInOut;" data-transform_out="y:[100%];s:1000;s:1000;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="500" data-responsive_offset="on">
                                        <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" width="260" height="303" data-ww="260px" data-hh="303px" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/ipad.png">
                                    </div>

                                    <div class="tp-caption tp-resizeme" data-x="281" data-y="390" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power2.easeInOut;" data-transform_out="y:[100%];s:1000;s:1000;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="500" data-responsive_offset="on">
                                        <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" width="221" height="228" data-ww="221px" data-hh="228px" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/iphone.png">
                                    </div>

                                    <div class="tp-caption tp-resizeme" data-x="768" data-y="276" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:[100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="500" data-responsive_offset="on">
                                        <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" width="694" height="347" data-ww="694px" data-hh="347px" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/macbook.png">
                                    </div>
                                </li>

                                <li data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="" data-rotate="0" data-saveperformance="off" data-title="Slide">

                                    <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/slider_1920x657.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg">


                                    <div class="tp-caption tp-resizeme" data-x="-191" data-y="46" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="x:-50px;opacity:0;s:300;s:300;" data-start="500" data-responsive_offset="on">
                                        <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/dummy.png" alt="" width="704" height="523" data-ww="704px" data-hh="523px" data-lazyload="<?php echo SITE_URL . 'assets/frontend/'; ?>images/slideshow/rev_img_home2.png">
                                    </div>

                                    <div class="tp-caption home1-small-black tp-resizeme" data-x="610" data-y="183" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                                        the best products 2015
                                    </div>

                                    <div class="tp-caption primary-button rev-btn" data-x="610" data-y="402" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bs:solid;bw:1px;cursor:pointer;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off">
                                        shop now
                                    </div>

                                    <div class="tp-caption home1-primary tp-resizeme" data-x="610" data-y="227" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                                        Built-in speakerphone
                                    </div>

                                    <div class="tp-caption home3-small-black tp-resizeme" data-x="610" data-y="296" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;" data-transform_out="y:[100%];rZ:0deg;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                                        Suscipit aliquam lorem ornare consectetur integer urna<br> fermentum venenatis, molestie a non phasellus feugiat curae nam<br> orci convallis
                                    </div>
                                </li>
                            </ul>
                            <div class="tp-bannertimer tp-bottom"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row row-fluid pt-6 pb-6">
                    <div class="text-center col-sm-3">
                        <div class="box-ft box-ft-5 black">
                            <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/thumb_270x470.jpg" alt="">
                            <a href="javascript:;">
                                <span class="bof-tf-title-wrap">
                                    <span class="bof-tf-title-wrap-2">
                                        <span class="bof-tf-title">iPad Pro</span>
                                        <span class="bof-tf-sub-title">Thin.Light.Epic</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-ft box-ft-5">
                            <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/thumb_570x470.jpg" alt="">
                            <a href="javascript:;">
                                <span class="bof-tf-title-wrap">
                                    <span class="bof-tf-title-wrap-2">
                                        <span class="bof-tf-title">Accessories</span>
                                        <span class="bof-tf-sub-title">
                                            Personalize your iPad with casesand covers. 
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="box-ft box-ft-5 mb-3">
                            <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/thumb_270x220.jpg" alt="">
                            <a href="javascript:;">
                                <span class="bof-tf-title-wrap">
                                    <span class="bof-tf-title-wrap-2">
                                        <span class="bof-tf-title">Mixr</span>
                                        <span class="bof-tf-sub-title">Sync your sound. And your style. </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                        <div class="box-ft box-ft-5">
                            <img src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/thumb_270x220.jpg" alt="">
                            <a href="javascript:;">
                                <span class="bof-tf-title-wrap">
                                    <span class="bof-tf-title-wrap-2">
                                        <span class="bof-tf-title">Mac Pro</span>
                                        <span class="bof-tf-sub-title">Starting at $2,999</span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row shipping-policy">
                    <div class="policy-featured-col col-md-4 col-sm-6">
                        <i class="fa fa-money"></i>
                        <h4 class="policy-featured-title">
                            100% <br> return money
                        </h4>
                        free return standard order in 30 days 
                    </div>
                    <div class="policy-featured-col col-md-4 col-sm-6">
                        <i class="fa fa-globe"></i>
                        <h4 class="policy-featured-title">
                            world wide <br> delivery
                        </h4>
                        free ship for payment over $100 
                    </div>
                    <div class="policy-featured-col col-md-4 col-sm-6">
                        <i class="fa fa-clock-o"></i>
                        <h4 class="policy-featured-title">
                            24h <br> shipment 
                        </h4>
                        for standard package 
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row row-fluid mb-10">
                    <div class="col-sm-12">
                        <div class="caroufredsel product-slider nav-position-center" data-height="variable" data-visible-min="1" data-responsive="1" data-infinite="1" data-autoplay="0">
                            <div class="product-slider-title">
                                <h3 class="el-heading">New Arrivals</h3>
                            </div>
                            <div class="caroufredsel-wrap">

                                <div class="commerce columns-4">
                                    <ul class="products columns-4" data-columns="4">
                                        <?php
                                        $allphones = $user_obj->getPhonesWithLimit(10);
                                        foreach ($allphones as $key => $phone) :
                                            ?>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">

                                                                <?php if (empty($phone["phone_image"])) { ?>
                                                                    <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/products/product_328x328.jpg" alt=""/></a>
                                                                    </div>
                                                                    <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/products/product_328x328alt.jpg" alt=""/></a>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . "uploads/" . $phone["phone_image"]; ?>" alt=""/></a>
                                                                    </div>
                                                                    <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . "uploads/" . $phone["phone_image"]; ?>" alt=""/></a>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><?php echo $phone["phone_name"]; ?></a>
                                                                    </h3>
                                                                    <div class="info-price">
                                                                        <span class="price">
                                                                            <del><span class="amount">£<?php echo ($phone["phone_price"]) ? $phone["phone_price"] : "10.00"; ?></span></del> <ins><span class="amount">£<?php echo ($phone["phone_price"]) ? $phone["phone_price"] : "10.00"; ?></span></ins>
                                                                        </span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>" class="add_to_cart_button">
                                                                                View Mobile
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="compareBox">
                                                                        <input type="checkbox" onclick="addtocompare(<?php echo $phone["phone_id"]; ?>)" />
                                                                        <span class="compare">Add to compare</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <a href="javascript:;" class="caroufredsel-prev"></a>
                                <a href="javascript:;" class="caroufredsel-next"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-full">
                <div class="row row-fluid custom-bg-2">
                    <div class="container">
                        <div class="col-sm-7 pt-12">
                            <p class="white italic size-15 mb-0">Connects wirelessly</p>
                            <h2 class="custom_heading white mt-0">Immaculate sound</h2>
                            <p class="white">Lorem ipsum dolor sit amet, natum aeterno sanctus ei per, fastidii torquatos nam ex. Amet vitae prodesset ut qui, labores civibus appellantur pri ei, pro cu tation dissentias. An per quando ornatus platonem, suas prodesset vel ad. Quas laoreet cotidieque cum ut, vix et insolens explicari corrumpit. Simul commodo et has, te tempor recusabo mea, eam sumo fabulas definiebas eu. No scripta legendos liberavisse vis.</p>
                        </div>
                        <div class="col-sm-5 pb-3">
                            <div class="special-product">
                                <div class="special-product-wrap">
                                    <div class="special-product-image">
                                        <a href="javascript:;">
                                            <img width="470" height="470" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/thumb_470x470.jpg" alt="special_product"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once './includes/frontend_common/footer.php'; ?>
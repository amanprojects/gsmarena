<?php
include_once '../config.php';
include_once '../classes/core.php';
(empty($_SESSION["user"])) ? $user_obj->redirect(SITE_URL_DASHBOARD . "login.php") : "";
include_once '../includes/backend_common/header.php';

(empty($_REQUEST["id"])) ? $user_obj->redirect(SITE_URL_DASHBOARD . "add.php") : "";

$edit_id = base64_decode($_REQUEST["id"]);
$getPhoneDetails = $user_obj->getPhoneDetails($edit_id);
?>
<div class="page-container row-fluid">
    <?php require_once '../includes/backend_common/sidebar.php'; ?>
    <a href="#" class="scrollup">Scroll</a>

    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content">
            <ul class="breadcrumb">
                <li>
                    <p>All Phones</p>
                </li>
                <li><a href="<?php echo SITE_URL_DASHBOARD . 'add.php'; ?>" class="active">Add New Phone</a> </li>
            </ul>
            <div class="row-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="grid simple">
                            <div class="grid-title no-border">
                                <h3>Add <span class="semi-bold">Phone Details</span></h3>
                            </div>
                            <div class="grid-body no-border">
                                <div class="row">
                                    <form method="POST" enctype="multipart/form-data">
                                        <div class="col-md-6">
                                            <h4>Phone <span class="semi-bold">Specification</span></h4>
                                            <br>
                                            <?php if (!empty($edit_id)) : ?>
                                                <input type="hidden" class="form-control" name="phone_id" value="<?php echo $edit_id; ?> ">
                                            <?php endif; ?>

                                            <div class="form-group">
                                                <label class="form-label">Brand</label>
                                                <div class="controls">						 
                                                    <select name="phone_category_id" class="form-control">
                                                        <?php
                                                        $categoies = $user_obj->getPhoneCategories();
                                                        foreach ($categoies as $category) {
                                                            ?>
                                                            <option value="<?php echo $category["category_id"]; ?>"><?php echo $category["category_name"]; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Name</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_name" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_name"] : ""; ?>">					                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Price</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_price"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_price"] : ""; ?>">					                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Camera</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_camera"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_camera"] : ""; ?>">							                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Ram</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_ram"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_ram"] : ""; ?>">							                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Battery</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_battery" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_battery"] : ""; ?>">							                        
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-6">
                                            <h4><span class="semi-bold">&nbsp;</span></h4>
                                            <br>

                                            <div class="form-group">
                                                <label class="form-label">Image</label>
                                                <div class="controls">						 
                                                    <input type="file" name="phone_image" style="display:none;" onchange="readFile(event)">
                                                    <img id="existImage" style="cursor: pointer;" src="<?php echo SITE_URL; ?>/assets/img/sample.jpg" onclick="$('input[name=phone_image]').click();"/>
                                                    <img id="output" width="100%" height="328"/>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <h4>Launch <span class="semi-bold"></span></h4>
                                            <br>
                                            <div class="form-group">
                                                <label class="form-label">Technology</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_technology" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_technology"] : ""; ?>">							                        
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Announced</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_announced" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_announced"] : ""; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Status</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_status" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_status"] : ""; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h4>Body <span class="semi-bold"></span></h4>
                                            <br>
                                            <div class="form-group">
                                                <label class="form-label">Dimensions</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_dimensions" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_dimensions"] : ""; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Weight</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_weight"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_weight"] : ""; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">SIM</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_sim" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_sim"] : ""; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h4>Display <span class="semi-bold"></span></h4>
                                            <br>

                                            <div class="form-group">
                                                <label class="form-label">Type</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_type"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_type"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Size</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_size"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_size"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Resolution</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_resolution"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_resolution"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Multitouch</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_multitouch"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_multitouch"] : ""; ?>">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <h4>Platform <span class="semi-bold"></span></h4>
                                            <br>

                                            <div class="form-group">
                                                <label class="form-label">OS</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_os"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_os"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Chipset</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_chipset" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_chipset"] : ""; ?>">						                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">CPU</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_cpu"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_cpu"] : ""; ?>">								                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">GPU</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_gpu"   value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_gpu"] : ""; ?>">								                        
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <h4>Camera <span class="semi-bold"></span></h4>
                                            <br>

                                            <div class="form-group">
                                                <label class="form-label">Camera</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_camera"   value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_camera"] : ""; ?>">						                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Features</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_features"   value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_features"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Video</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_video" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_video"] : ""; ?>">						                        
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Secondary</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_secondary" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_secondary"] : ""; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h4>Features <span class="semi-bold"></span></h4>
                                            <br>

                                            <div class="form-group">
                                                <label class="form-label">Sensors</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_sensors" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_sensors"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Messaging</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_messaging"  value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_messaging"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Browser</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_browser" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_browser"] : ""; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Java</label>
                                                <div class="controls">						 
                                                    <input type="text" class="form-control" name="phone_java" value="<?php echo (!empty($edit_id)) ? $getPhoneDetails["phone_java"] : ""; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-default pull-right" name="action" value="updatePhone">
                                                <?php echo (!empty($edit_id)) ? "Update" : "Add New"; ?> Phone
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="addNewRow"></div>
    </div>
</div>
<script type="text/javascript">
    function readFile(event) {
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById("output");
            $("#existImage").css("display", "none");
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }

</script>
<?php include_once '../includes/backend_common/footer.php'; ?>

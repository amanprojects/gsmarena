<?php
include_once '../config.php';
include_once '../classes/core.php';
(empty($_SESSION["user"])) ? $user_obj->redirect(SITE_URL_DASHBOARD . "login.php") : "";
include_once '../includes/backend_common/header.php';

$phoneListing = $user_obj->getAllPhones();
?>
<div class="page-container row-fluid">
    <?php require_once '../includes/backend_common/sidebar.php'; ?>
    <a href="#" class="scrollup">Scroll</a>

    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content">
            <ul class="breadcrumb">
                <li>
                    <p>All Phones</p>
                </li>
                <li><a href="<?php echo SITE_URL_DASHBOARD . 'allphones.php'; ?>" class="active">Phone List</a> </li>
            </ul>
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4><span class="semi-bold"></span></h4>
                        </div>
                        <div class="grid-body">
                            <table class="table table-hover table-condensed" id="example">
                                <thead>
                                    <tr>
                                        <th>Phone Category</th>
                                        <th>Phone Image</th>
                                        <th>Phone Name</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($phoneListing as $details) {
                                        $categoryName = $user_obj->getCategoryName($details["phone_category_id"]);
                                        ?>
                                        <tr>
                                            <td class="v-align-middle"><?php echo $categoryName["category_name"]; ?></td>
                                            <td class="v-align-middle">
                                                <img width="100" height="auto" src="<?php echo SITE_URL . "uploads/" . $details["phone_image"]; ?>" alt="image" />
                                            </td>
                                            <td class="v-align-middle"><?php echo $details["phone_name"]; ?></td>
                                            <td class="v-align-middle"><?php echo $details["phone_price"]; ?></td>
                                            <td><a href="<?php echo SITE_URL_DASHBOARD . "update.php?id=" . base64_encode($details["phone_id"]); ?>">Edit</a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="addNewRow"></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
//        $("#example").DataTable();
    });
</script>
<?php include_once '../includes/backend_common/footer.php'; ?>

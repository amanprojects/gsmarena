<?php
include_once '../config.php';
include_once '../classes/core.php';
(empty($_SESSION["user"])) ? $user_obj->redirect(SITE_URL_DASHBOARD . "login.php") : "";
include_once '../includes/backend_common/header.php';
?>
<!-- BEGIN CONTAINER -->
<div class="page-container row">
    <?php include_once '../includes/backend_common/sidebar.php'; ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title">
                <h3>Dashboard </h3>
            </div>
            <div id="container">
                <div class="row 2col">
                    <div class="col-md-8 spacing-bottom">

                    </div>
                </div>
            </div>
            <!-- END PAGE -->
        </div>
    </div>
    <!-- END CONTAINER -->
</div>

<?php include_once '../includes/backend_common/footer.php'; ?>
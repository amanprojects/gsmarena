<?php
include_once '../config.php';
include_once '../classes/core.php';

(!empty($_SESSION["user"])) ? $user_obj->redirect(SITE_URL_DASHBOARD . "index.php") : "";

include_once '../includes/backend_login/header.php';
?>
<div class="container">
    <div class="row login-container column-seperation">  
        <div class="col-md-5 col-md-offset-4"> <br>
            <?php if (!empty($_SESSION["errors"]["user_login"])) { ?>
                <div class="row">
                    <div class="form-group col-md-10">
                        <p class="alert-danger"><?php echo $_SESSION["errors"]["user_login"]; ?></p>
                    </div>
                </div>
                <?php
                unset($_SESSION["errors"]["user_login"]);
            }
            ?>
            <form id="login-form" class="login-form" method="post">
                <input type="hidden" name="action" value="login" />
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-label">Username</label>
                        <div class="controls">
                            <div class="input-with-icon  right">                                       
                                <i class=""></i>
                                <input type="text" name="username" id="txtusername" class="form-control">                                 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-10">
                        <label class="form-label">Password</label>
                        <span class="help"></span>
                        <div class="controls">
                            <div class="input-with-icon  right">                                       
                                <i class=""></i>
                                <input type="password" name="password" id="txtpassword" class="form-control">                                 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="control-group  col-md-10">
                        <div class="checkbox checkbox check-success"> <a href="#">Trouble login in?</a>&nbsp;&nbsp;
                            <input type="checkbox" id="checkbox1" value="1">
                            <label for="checkbox1">Keep me reminded </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once '../includes/backend_login/footer.php'; ?>
<?php
include_once '../config.php';
include_once '../classes/core.php';
(empty($_SESSION["user"])) ? $user_obj->redirect(SITE_URL_DASHBOARD . "login.php") : "";
include_once '../includes/backend_common/header.php';

$phoneListing = $user_obj->getAllPhones();
?>
<div class="page-container row-fluid">
    <?php require_once '../includes/backend_common/sidebar.php'; ?>
    <a href="#" class="scrollup">Scroll</a>

    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content">
            <ul class="breadcrumb">
                <li>
                    <p>YOU ARE HERE</p>
                </li>
                <li><a href="#" class="active">Tables</a> </li>
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
                <h3>Advance - <span class="semi-bold">Tables</span></h3>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Table <span class="semi-bold">Styles</span></h4>
                        </div>
                        <div class="grid-body ">
                            <table class="table table-hover table-condensed" id="example">
                                <thead>
                                    <tr>
                                        <th>Phone Category</th>
                                        <th>Phone Name</th>
                                        <th>Price</th>
                                        <th>Camera</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($phoneListing as $details) { ?>
                                        <tr>
                                            <td class="v-align-middle"><?php echo $details["phone_category_id"]; ?></td>
                                            <td class="v-align-middle"><?php echo $details["phone_name"]; ?></td>
                                            <td class="v-align-middle"><?php echo $details["phone_price"]; ?></td>
                                            <td><span class="muted"><?php echo $details["phone_camera"]; ?></span></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="addNewRow"></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
//        $("#example").DataTable();
    });
</script>
<?php include_once '../includes/backend_common/footer.php'; ?>

-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2017 at 08:08 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gsmarena`
--

-- --------------------------------------------------------

--
-- Table structure for table `gs_admin`
--

CREATE TABLE IF NOT EXISTS `gs_admin` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gs_admin`
--

INSERT INTO `gs_admin` (`user_id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `gs_category`
--

CREATE TABLE IF NOT EXISTS `gs_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `gs_category`
--

INSERT INTO `gs_category` (`category_id`, `category_name`, `is_active`, `is_delete`) VALUES
(1, 'Samsung', 0, 0),
(2, 'HTC', 0, 0),
(3, 'Motorola', 0, 0),
(4, 'LG', 0, 0),
(5, 'Apple', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gs_phonedetails`
--

CREATE TABLE IF NOT EXISTS `gs_phonedetails` (
  `phone_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_category_id` int(11) NOT NULL,
  `phone_image` varchar(255) NOT NULL,
  `phone_price` varchar(255) NOT NULL,
  `phone_name` varchar(255) NOT NULL,
  `phone_camera` varchar(255) NOT NULL,
  `phone_ram` varchar(255) NOT NULL,
  `phone_battery` varchar(255) NOT NULL,
  `phone_technology` varchar(255) NOT NULL,
  `phone_announced` varchar(255) NOT NULL,
  `phone_status` varchar(255) NOT NULL,
  `phone_dimensions` varchar(255) NOT NULL,
  `phone_weight` varchar(255) NOT NULL,
  `phone_sim` varchar(255) NOT NULL,
  `phone_type` varchar(255) NOT NULL,
  `phone_size` varchar(255) NOT NULL,
  `phone_resolution` varchar(255) NOT NULL,
  `phone_multitouch` varchar(255) NOT NULL,
  `phone_os` varchar(255) NOT NULL,
  `phone_chipset` varchar(255) NOT NULL,
  `phone_cpu` varchar(255) NOT NULL,
  `phone_gpu` varchar(255) NOT NULL,
  `phone_features` varchar(255) NOT NULL,
  `phone_video` varchar(255) NOT NULL,
  `phone_secondary` varchar(255) NOT NULL,
  `phone_sensors` varchar(255) NOT NULL,
  `phone_messaging` varchar(255) NOT NULL,
  `phone_browser` varchar(255) NOT NULL,
  `phone_java` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`phone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gs_phonedetails`
--

INSERT INTO `gs_phonedetails` (`phone_id`, `phone_category_id`, `phone_image`, `phone_price`, `phone_name`, `phone_camera`, `phone_ram`, `phone_battery`, `phone_technology`, `phone_announced`, `phone_status`, `phone_dimensions`, `phone_weight`, `phone_sim`, `phone_type`, `phone_size`, `phone_resolution`, `phone_multitouch`, `phone_os`, `phone_chipset`, `phone_cpu`, `phone_gpu`, `phone_features`, `phone_video`, `phone_secondary`, `phone_sensors`, `phone_messaging`, `phone_browser`, `phone_java`, `is_active`, `is_delete`) VALUES
(1, 1, '1492312606.8133image001 (1).jpg', '1200', 'Galaxy S7', '', '2GB', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0),
(2, 1, '1492312557.0688Untitled.png', '10', 'G4 Plus', '', '', '', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0),
(3, 1, '1492312103.582.jpg', '123', 'HTCCC', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0),
(4, 1, '1492312637.6292Untitled.png', '1200', 'gs8', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

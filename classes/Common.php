<?php

/**
 * Description of User
 *
 * @author Administrator
 */
class Common extends Database {

    public $db_connection;
    public $userID;

    /**
     * 
     * @param type $username
     * @param type $password
     * @param type $status
     */
    function __construct($db) {
        $this->db_connection = $db;
    }

    /**
     * Redirect function
     * @param type $path
     */
    public function redirect($path, $time) {
        if (isset($time) && !empty($time)) {
            header("refresh:$time;url=$path");
        } else {
            header("location:$path");
            die();
        }
    }

    /**
     * To check the user login
     * @return boolean
     */
    public function userLogin($data) {
        $query = "SELECT * FROM gs_admin WHERE username = '" . trim($data["username"]) . "' AND password = '" . trim($data["password"]) . "' ";
        if ($this->checkRecordExists($query)) {
            $_SESSION["user"]["login"] = "yes";
            $this->redirect(SITE_URL_DASHBOARD . "index.php");
        } else {
            $_SESSION["errors"]["user_login"] = "Please check your credentials";
        }
    }

    public function addPhoneDetails($data) {

        $tableName = "gs_phonedetails";
        unset($data["action"]);

        $target_dir = UPLOADING_PATH . "uploads/";
        $fileName = microtime(true) . basename($_FILES["phone_image"]["name"]);
        $target_file = $target_dir . $fileName;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);


        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $_SESSION["errors"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            $_SESSION["errors"] = "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($_FILES["phone_image"]["tmp_name"], $target_file)) {
//                echo "The file " . basename($_FILES["phone_image"]["name"]) . " has been uploaded.";
            } else {
//                echo "Sorry, there was an error uploading your file.";
            }
        }

        $data["phone_image"] = $fileName;
        $this->insertRecords($tableName, $data);
        $this->redirect(SITE_URL_DASHBOARD . "allphones.php");
    }

    public function updatePhoneDetails($data) {

        $tableName = "gs_phonedetails";
        unset($data["action"]);

        $target_dir = UPLOADING_PATH . "uploads/";
        if (!empty($_FILES["phone_image"]["name"])) {
            $fileName = microtime(true) . basename($_FILES["phone_image"]["name"]);
            $target_file = $target_dir . $fileName;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);


            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
//                $_SESSION["errors"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
//                $_SESSION["errors"] = "Sorry, your file was not uploaded.";
            } else {
                echo $target_file;
                if (move_uploaded_file($_FILES["phone_image"]["tmp_name"], $target_file)) {
//                    echo "The file " . basename($_FILES["phone_image"]["name"]) . " has been uploaded.";
                } else {
//                    echo "Sorry, there was an error uploading your file.";
                }
            }


            $data["phone_image"] = $fileName;
        } else {
            
        }
        $phone_id = $data["phone_id"];
        unset($data["phone_id"]);
        $whereCondition = " phone_id = $phone_id ";
        $this->updateRecordsArray($tableName, $data, $whereCondition);
        $this->redirect(SITE_URL_DASHBOARD . "allphones.php");
    }

    public function getPhoneCategories() {
        $query = "SELECT * FROM gs_category WHERE is_active = 0 AND is_delete = 0";
        if ($this->checkRecordExists($query)) {
            return $this->getRecords($query);
        }
    }

    public function getBrandName($bid) {
        $query = "SELECT * FROM gs_category WHERE category_id= $bid AND is_active = 0 AND is_delete = 0";
        if ($this->checkRecordExists($query)) {
            return $this->getRecord($query);
        }
    }

    public function getCategoryName($category_id) {
        $query = "SELECT category_name FROM gs_category WHERE category_id= '" . $category_id . "' AND is_active = 0 AND is_delete = 0";
        if ($this->checkRecordExists($query)) {
            return $this->getRecord($query);
        }
    }

    public function getPhonesForCompare($phoneIDs) {

        $query = "SELECT * FROM gs_phonedetails WHERE phone_id = '" . $phoneIDs . "' ";
        if ($this->checkRecordExists($query)) {
            $records = $this->getRecord($query);
            return $records;
        }
    }

    public function getPhonesWithLimit($limit) {
        $query = "SELECT * FROM gs_phonedetails where is_active = 0 AND is_delete=0 LIMIT 0, $limit";
        if ($this->checkRecordExists($query)) {
            return $this->getRecords($query);
        }
    }

    public function getPhonesBybrand($bid) {
        $query = "SELECT * FROM gs_phonedetails where phone_category_id = $bid AND is_active = 0 AND is_delete=0";
        if ($this->checkRecordExists($query)) {
            return $this->getRecords($query);
        }
    }

    public function getPhoneDetails($phoneID) {
        $query = "SELECT * FROM gs_phonedetails where phone_id = '" . $phoneID . "' AND is_active = 0 AND is_delete=0";
        if ($this->checkRecordExists($query)) {
            return $this->getRecord($query);
        }
    }

    public function getAllPhones() {
        $query = "SELECT * FROM gs_phonedetails where is_delete = 0 AND is_active = 0";
        if ($this->checkRecordExists($query)) {
            return $this->getRecords($query);
        }
    }

    public function handleRequest() {
        $post = $_POST;
        if (isset($post["action"])) {
            switch ($post["action"]) {
                case "login":
                    $this->userLogin($post);
                    break;

                case "addPhone":
                    $this->addPhoneDetails($post);
                    break;

                case "updatePhone":
                    $this->updatePhoneDetails($post);
                    break;

                default:
                    break;
            }
        }
    }

}

<?php
include_once 'config.php';
include_once 'classes/core.php';

include_once 'includes/frontend_common/header.php';

$brandName = $user_obj->getBrandName(base64_decode($_REQUEST["bid"]));
?>
<div class="content-container no-padding">
    <div class="container-full">
        <div class="main-content">
            <div class="container">
                <div class="row row-fluid mb-10">
                    <div class="col-sm-12">
                        <div class="caroufredsel product-slider nav-position-center" data-height="variable" data-visible-min="1" data-responsive="1" data-infinite="1" data-autoplay="0">
                            <div class="product-slider-title">
                                <h3 class="el-heading"><?php echo $brandName["category_name"]; ?></h3>
                            </div>
                            <?php
                            $allphones = $user_obj->getPhonesBybrand(base64_decode($_REQUEST["bid"]));
                            if (!empty($allphones)) :
                                ?>

                                <div class="caroufredsel-wrap">

                                    <div class="commerce columns-4">
                                        <ul class="products columns-4" data-columns="4">
                                            <?php
                                            foreach ($allphones as $key => $phone) :
                                                ?>
                                                <li class="product product-no-border style-2">
                                                    <div class="product-container">
                                                        <figure>
                                                            <div class="product-wrap">
                                                                <div class="product-images">

                                                                    <?php if (empty($phone["phone_image"])) { ?>
                                                                        <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                            <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/products/product_328x328.jpg" alt=""/></a>
                                                                        </div>
                                                                        <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                            <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/products/product_328x328alt.jpg" alt=""/></a>
                                                                        </div>
                                                                    <?php } else { ?>
                                                                        <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                            <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . "uploads/" . $phone["phone_image"]; ?>" alt=""/></a>
                                                                        </div>
                                                                        <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                            <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><img width="450" height="450" src="<?php echo SITE_URL . "uploads/" . $phone["phone_image"]; ?>" alt=""/></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <figcaption>
                                                                <div class="shop-loop-product-info">
                                                                    <div class="info-content-wrap">
                                                                        <h3 class="product_title">
                                                                            <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>"><?php echo $phone["phone_name"]; ?></a>
                                                                        </h3>
                                                                        <div class="info-price">
                                                                            <span class="price">
                                                                                <del><span class="amount">£<?php echo ($phone["phone_price"]) ? $phone["phone_price"] : "10.00"; ?></span></del> <ins><span class="amount">£<?php echo ($phone["phone_price"]) ? $phone["phone_price"] : "10.00"; ?></span></ins>
                                                                            </span>
                                                                        </div>
                                                                        <div class="loop-action">
                                                                            <div class="loop-add-to-cart">
                                                                                <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($phone["phone_id"]) ?>" class="add_to_cart_button">
                                                                                    View Mobile
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="compareBox">
                                                                            <input type="checkbox" onclick="addtocompare(<?php echo $phone["phone_id"]; ?>)" />
                                                                            <span class="compare">Add to compare</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </figcaption>
                                                        </figure>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>

                                        </ul>
                                    </div>
                                    <a href="javascript:;" class="caroufredsel-prev"></a>
                                    <a href="javascript:;" class="caroufredsel-next"></a>
                                </div>
                                <?php
                            else:
                                ?>
                                <div style="text-align: center;">
                                    <h2>Sorry currently, no phones are available.</h2>
                                </div>
                            <?php endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once './includes/frontend_common/footer.php'; ?>
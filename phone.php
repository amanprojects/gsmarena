<?php
include_once 'config.php';
include_once 'classes/core.php';

include_once 'includes/frontend_common/header.php';

$phone = $user_obj->getPhoneDetails(base64_decode($_REQUEST["pid"]));
//echo "<pre>";
//print_r($brandName);
//echo "</pre>";
?>
<div class="heading-container">
    <div class="container heading-standar">
        <div class="page-breadcrumb">
            <ul class="breadcrumb">
                <li>
                    <span>
                        <a class="home" href="#">
                            <span>Home</span>
                        </a>
                    </span>
                </li>
                <li>
                    <span>Phones</span>
                </li>
                <li>
                    <span><?php echo $brandName["phone_name"]; ?></span>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="content-container no-padding">
    <div class="container-full">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content">
                    <div class="commerce">
                        <div class="style-1 product">
                            <div class="container">
                                <div class="row summary-container">
                                    <div class="col-md-7 col-sm-6 entry-image">
                                        <div class="single-product-images">

                                            <div class="single-product-images-slider">
                                                <div class="caroufredsel product-images-slider" data-synchronise=".single-product-images-slider-synchronise" data-scrollduration="500" data-height="variable" data-scroll-fx="none" data-visible="1" data-circular="1" data-responsive="1">
                                                    <div class="caroufredsel-wrap">
                                                        <ul class="caroufredsel-items">
                                                            <li class="caroufredsel-item">
                                                                <?php if (empty($phone["phone_image"])) { ?>
                                                                    <a href="<?php echo SITE_URL . "assets/frontend/" ?>images/products/detail/detail_800x800.jpg" data-rel="magnific-popup-verticalfit">
                                                                        <img width="600" height="685" src="<?php echo SITE_URL . "assets/frontend/" ?>images/products/detail/detail_800x800.jpg" alt=""/>
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <a href="<?php echo SITE_URL . "uploads/" . $phone["phone_image"]; ?>" data-rel="magnific-popup-verticalfit">
                                                                        <img width="600" height="685" src="<?php echo SITE_URL . "uploads/" . $phone["phone_image"]; ?>" alt=""/>
                                                                    </a>
                                                                <?php } ?>
                                                            </li>
                                                        </ul>
                                                        <a href="#" class="caroufredsel-prev"></a>
                                                        <a href="#" class="caroufredsel-next"></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-6 entry-summary">
                                        <div class="summary">
                                            <h1 class="product_title entry-title"><?php echo $phone["phone_name"]; ?></h1>
                                            <p class="price">
                                                <ins>
                                                    <span class="amount"><?php echo SIGN . $phone["phone_price"]; ?></span>
                                                </ins>
                                            </p>
                                            <?php if (!empty($phone["phone_technology"])) { ?>
                                                <div class="product-excerpt">
                                                    <p><?php echo $phone["phone_technology"]; ?></p>
                                                </div>
                                            <?php } ?>

                                            <div class="clear"></div>
                                            <div class="product_meta">
                                                <span class="posted_in">
                                                    Brand: 
                                                    <a href="<?php echo SITE_URL . "brands.php?bid=" . $phone["phone_category_id"]; ?>">
                                                        <?php
                                                        $phoneCatArray = $user_obj->getCategoryName($phone["phone_category_id"]);
                                                        echo $phoneCatArray["category_name"];
                                                        ?>
                                                    </a>
                                                </span>
                                            </div>
                                            <div class="share-links">
                                                <div class="share-icons">
                                                    <span class="facebook-share">
                                                        <a href="#" title="Share on Facebook">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </span>
                                                    <span class="twitter-share">
                                                        <a href="#" title="Share on Twitter">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </span>
                                                    <span class="google-plus-share">
                                                        <a href="#" title="Share on Google +">
                                                            <i class="fa fa-google-plus"></i>
                                                        </a>
                                                    </span>
                                                    <span class="linkedin-share">
                                                        <a href="#" title="Share on Linked In">
                                                            <i class="fa fa-linkedin"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="commerce-tab-container">
                                <div class="container">
                                    <div class="col-md-12">
                                        <div class="tabbable commerce-tabs">
                                            <ul class="nav nav-tabs">
                                                <li class="vc_tta-tab active">
                                                    <a data-toggle="tab" href="#tab-1">Description</a>
                                                </li>
                                                <li class="vc_tta-tab">
                                                    <a data-toggle="tab" href="#tab-2">Reviews</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab-1" class="tab-pane fade in active">
                                                    <h2>Product Description</h2>
                                                    <h3>Nullam vulputate erat id enim lacinia</h3>
                                                    <h3></h3>
                                                    <p>Vel rutrum odio bibendum. Vestibulum quis metus euismod, porta odio et, lacinia eros. Vestibulum vel lobortis ligula, non mollis diam. Donec eu urna quis nibh consectetur pharetra eget vitae dolor. Duis volutpat orci at</p>
                                                    <p>Curabitur rutrum tristique arcu eget tincidunt. Nullam cursus viverra condimentum. Fusce vel nisi sem. Suspendisse sit amet mauris laoreet velit pretium tempus in quis purus.</p>
                                                    <h3></h3>
                                                    <p>Nullam molestie vulputate magna ac tempus. Quisque ac nibh finibus, tempor nunc a, euismod nunc. Nunc lectus magna, mattis eget libero eu, pharetra dapibus tellus. Aliquam dignissim lacus arcu, eu gravida purus rhoncus nec. Aenean porta tempus diam sit amet consequat. Morbi condimentum hendrerit aliquam. Sed at neque faucibus</p>
                                                    <h3></h3>
                                                    <h3></h3>
                                                    <h3>Aenean aliquet condimentum augue, ut tempor turpis sollicitudin in.</h3>
                                                    <p>Nunc vitae odio mollis, euismod mauris at, finibus felis. Phasellus vestibulum, sem at varius imperdiet, velit risus laoreet tortor, in feugiat massa augue sed nibh. Ut fermentum, ligula eget dictum vulputate, orci risus viverra nulla, non posuere metus orci quis mauris. Vivamus aliquam, purus sit amet ultricies dignissim, libero massa rhoncus mi, et imperdiet mauris mi in leo. Ut viverra, erat non rutrum suscipit, nunc purus porta odio, sit amet egestas ex tellus quis nisl. Nullam vitae egestas lectus. Duis faucibus sagittis nunc non porta. Ut eget tempus justo. Donec iaculis id nibh at rhoncus. Nam sed ex lectus. Sed aliquam imperdiet libero ut sollicitudin.</p>
                                                </div>
                                                <div id="tab-2" class="tab-pane fade">
                                                    <div id="comments" class="comments-area">
                                                        <h2 class="comments-title">There are <span>3</span> Comments</h2>
                                                        <ol class="comments-list">
                                                            <li class="comment">
                                                                <div class="comment-wrap">
                                                                    <div class="comment-img">
                                                                        <img alt="" src="http://placehold.it/80x80" class='avatar' height='80' width='80'/>
                                                                    </div>
                                                                    <div class="comment-block">
                                                                        <header class="comment-header">
                                                                            <cite class="comment-author">
                                                                                John Doe
                                                                            </cite>
                                                                            <div class="comment-meta">
                                                                                <span class="time">10 days ago</span>
                                                                            </div>
                                                                        </header>
                                                                        <div class="comment-content">
                                                                            <p>
                                                                                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
                                                                            </p>
                                                                            <span class="comment-reply">
                                                                                <a class='comment-reply-link' href='#'>Reply</a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <ol class="children">
                                                                    <li class="comment">
                                                                        <div class="comment-wrap">
                                                                            <div class="comment-img">
                                                                                <img alt="" src="http://placehold.it/80x80" class='avatar' height='80' width='80'/>
                                                                            </div>
                                                                            <div class="comment-block">
                                                                                <header class="comment-header">
                                                                                    <cite class="comment-author">
                                                                                        Jane Doe
                                                                                    </cite>
                                                                                    <div class="comment-meta">
                                                                                        <span class="time">10 days ago</span>
                                                                                    </div>
                                                                                </header>
                                                                                <div class="comment-content">
                                                                                    <p>
                                                                                        Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
                                                                                    </p>
                                                                                    <span class="comment-reply">
                                                                                        <a class='comment-reply-link' href='#'>Reply</a>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li> 
                                                                </ol> 
                                                            </li> 
                                                            <li class="comment">
                                                                <div class="comment-wrap">
                                                                    <div class="comment-img">
                                                                        <img alt="" src="http://placehold.it/80x80" class='avatar' height='80' width='80'/>
                                                                    </div>
                                                                    <div class="comment-block">
                                                                        <header class="comment-header">
                                                                            <cite class="comment-author">
                                                                                David Platt
                                                                            </cite>
                                                                            <div class="comment-meta">
                                                                                <span class="time">5 days ago</span>
                                                                            </div>
                                                                        </header>
                                                                        <div class="comment-content">
                                                                            <p>
                                                                                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
                                                                            </p>
                                                                            <span class="comment-reply">
                                                                                <a class='comment-reply-link' href='#'>Reply</a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li> 
                                                        </ol>
                                                        <div class="comment-respond" style="display: none;">
                                                            <h3 class="comment-reply-title">
                                                                <span>Leave your thought</span>
                                                            </h3>
                                                            <form class="comment-form">
                                                                <div class="row">
                                                                    <div class="comment-form-author col-sm-12">
                                                                        <input id="author" name="author" type="text" placeholder="Your name" class="form-control" value="" size="30" />
                                                                    </div>
                                                                    <div class="comment-form-email col-sm-12">
                                                                        <input id="email" name="email" type="text" placeholder="email@domain.com" class="form-control" value="" size="30" />
                                                                    </div>
                                                                    <div class="comment-form-comment col-sm-12">
                                                                        <textarea class="form-control" placeholder="Comment" id="comment" name="comment" cols="40" rows="6" ></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-submit">
                                                                    <a class="btn btn-default-outline btn-outline" href="#">
                                                                        <span>Post Comment</span>
                                                                    </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once './includes/frontend_common/footer.php'; ?>
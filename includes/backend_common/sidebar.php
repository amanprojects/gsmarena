<!-- BEGIN SIDEBAR -->
<div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <div class="user-info-wrapper">
            <div class="profile-wrapper"> <img src="<?php echo SITE_URL; ?>assets/img/profiles/avatar.jpg"  alt="" data-src="<?php echo SITE_URL; ?>assets/img/profiles/avatar.jpg" data-src-retina="<?php echo SITE_URL; ?>assets/img/profiles/avatar2x.jpg" width="69" height="69" /> </div>
            <div class="user-info">
                <div class="greeting">Welcome</div>
                <div class="username">John <span class="semi-bold">Smith</span></div>
                <div class="status">Status
                    <a href="#">
                        <div class="status-icon green"></div>
                        Online</a>  
                </div>
            </div>
        </div>
        <!-- END MINI-PROFILE -->
        <!-- BEGIN SIDEBAR MENU -->
        <p class="menu-title">BROWSE <span class="pull-right"></span></p>
        <ul>
            <li class="start"> 
                <a href="index.php"> 
                    <i class="icon-custom-home"></i> 
                    <span class="title">Dashboard</span>
                </a> 
            </li>
            <li class=""> 
                <a href="<?php echo SITE_URL; ?>"> 
                    <i class="fa fa-flag"></i>  
                    <span class="title">Frontend</span>
                </a>
            </li>
            <li class="start"> 
                <a href="<?php echo SITE_URL_DASHBOARD . "allphones.php"; ?>"> 
                    <i class="icon-custom-home"></i> 
                    <span class="title">All Phones</span> 
                    <span class="selected"></span> 
                    <span class="arrow"></span> 
                </a> 
                <ul class="sub-menu">
                    <li> <a href="<?php echo SITE_URL_DASHBOARD . "allphones.php" ?>"> Phones Listing </a> </li>
                    <li> <a href="<?php echo SITE_URL_DASHBOARD . "add.php" ?>"> Add Phone </a> </li>
                </ul>
            </li>
        </ul>

        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
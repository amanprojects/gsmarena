<!doctype html>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>My Site</title>
        <link rel="shortcut icon" href="<?php echo SITE_URL . 'assets/frontend/'; ?>images/favicon.ico">
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/bootstrap.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/swatches-and-photos.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/prettyPhoto.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/jquery.selectBox.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/commerce.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/custom.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='<?php echo SITE_URL; ?>assets/frontend/css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic%7CCrimson+Text:400,400italic,600,600italic,700,700italic' type='text/css' media='all'/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="offcanvas open">
            <div class="offcanvas-wrap">
                <div class="offcanvas-user clearfix">
                    <a class="offcanvas-user-account-link" href="<?php echo SITE_URL_DASHBOARD; ?>">
                        <i class="fa fa-user"></i> Login
                    </a>
                </div>

                <nav class="offcanvas-navbar">
                    <ul class="offcanvas-nav">
                        <li class="current-menu-item menu-item-has-children dropdown">
                            <a href="<?php echo SITE_URL; ?>" class="dropdown-hover">
                                <span class="underline">Home</span>
                            </a>
                        </li>

                        <li class="current-menu-item menu-item-has-children dropdown">
                            <a href="javascript:;" class="dropdown-hover">
                                <span class="underline">Brands</span> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <?php
                                $categoies = $user_obj->getPhoneCategories();
                                foreach ($categoies as $category) {
                                    ?>
                                    <li><a href="<?php echo SITE_URL . 'brands.php?bid=' . base64_encode($category["category_id"]); ?>"><?php echo $category["category_name"]; ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li><a href="<?php echo SITE_URL . "about-us.php"; ?>">About us</a></li>
                        <li><a href="<?php echo SITE_URL . "contact-us.php"; ?>">Contact Us</a></li>
                        <li><a href="<?php echo SITE_URL . "faq.php"; ?>">FAQ</a></li>
                        <li class="comparing">
                            <a href="<?php echo SITE_URL . "compare.php"; ?>">Compare&nbsp;
                                <span class="count"style="color: red; font-weight: bold;text-decoration: underline;">
                                    <?php echo (!empty($_COOKIE["comparing_products"])) ? $_COOKIE["comparing_products"] : ""; ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
            <div class="offcanvas-overlay"></div>
            <header class="header-container header-type-classic header-navbar-classic header-scroll-resize">
                <div class="topbar">
                    <div class="container topbar-wap">
                        <div class="row">
                            <div class="col-sm-6 col-left-topbar">
                                <div class="left-topbar">
                                    Shop unique and handmade items directly 
                                    <a href="#">About<i class="fa fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-sm-6 col-right-topbar">
                                <div class="right-topbar">
                                    <div class="user-login">
                                        <ul class="nav top-nav">
                                            <li><a  href="<?php echo SITE_URL_DASHBOARD; ?>"> Login </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar-container">
                    <div class="navbar navbar-default navbar-scroll-fixed">
                        <div class="navbar-default-wrap">
                            <div class="container">
                                <div class="row">
                                    <div class="navbar-default-col">
                                        <div class="navbar-wrap">
                                            <div class="navbar-header">
                                                <button type="button" class="navbar-toggle">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar bar-top"></span>
                                                    <span class="icon-bar bar-middle"></span>
                                                    <span class="icon-bar bar-bottom"></span>
                                                </button>
                                                <a class="navbar-search-button search-icon-mobile" href="#">
                                                    <i class="fa fa-search"></i>
                                                </a><!--
                                                <a class="cart-icon-mobile" href="#">
                                                    <i class="elegant_icon_bag"></i><span>0</span>
                                                </a>-->
                                                <a class="navbar-brand" href="./">
                                                    <img class="logo" alt="WOOW" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/logo.png">
                                                    <img class="logo-fixed" alt="WOOW" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/logo-fixed.png">
                                                    <img class="logo-mobile" alt="WOOW" src="<?php echo SITE_URL . 'assets/frontend/'; ?>images/logo-mobile.png">
                                                </a>
                                            </div>
                                            <nav class="collapse navbar-collapse primary-navbar-collapse">
                                                <ul class="nav navbar-nav primary-nav">
                                                    <li class="current-menu-item menu-item-has-children dropdown">
                                                        <a href="<?php echo SITE_URL; ?>" class="dropdown-hover">
                                                            <span class="underline">Home</span>
                                                        </a>
                                                    </li>

                                                    <li class="current-menu-item menu-item-has-children dropdown">
                                                        <a href="javascript:;" class="dropdown-hover">
                                                            <span class="underline">Brands</span> <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            foreach ($categoies as $category) {
                                                                ?>
                                                                <li><a href="<?php echo SITE_URL . 'brands.php?bid=' . base64_encode($category["category_id"]); ?>"><?php echo $category["category_name"]; ?></a></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </li>
                                                    <li><a href="<?php echo SITE_URL . "about-us.php"; ?>">About us</a></li>
                                                    <li><a href="<?php echo SITE_URL . "contact-us.php"; ?>">Contact Us</a></li>
                                                    <li><a href="<?php echo SITE_URL . "faq.php"; ?>">FAQ</a></li>

                                                    <li class="comparing">
                                                        <a href="<?php echo SITE_URL . "compare.php"; ?>">Compare&nbsp;
                                                            <span class="count"style="color: red; font-weight: bold;text-decoration: underline;">
                                                                <?php echo (!empty($_COOKIE["comparing_products"])) ? count($_COOKIE["comparing_products"]) : ""; ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                            <div class="header-right">
                                                <div class="navbar-search">
                                                    <a class="navbar-search-button" href="#">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                    <div class="search-form-wrap show-popup hide"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-search-overlay hide">
                            <div class="container">
                                <div class="header-search-overlay-wrap">
                                    <form class="searchform">
                                        <input type="search" class="searchinput" name="s" autocomplete="off" value="" placeholder="Search..."/>
                                    </form>
                                    <button type="button" class="close">
                                        <span aria-hidden="true" class="fa fa-times"></span>
                                        <span class="sr-only">Close</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
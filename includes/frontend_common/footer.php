<footer id="footer" class="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="footer-widget-wrap">
                <div class="row">
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget widget_text">
                            <div class="textwidget">
                                <ul class="address">
                                    <li>
                                        <i class="fa fa-home"></i>
                                        <h4>Address:</h4>
                                        <p>132 Jefferson Avenue, Suite 22, Redwood City, CA 94872, USA</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-mobile"></i>
                                        <h4>Phone:</h4>
                                        <p>(00) 123 456 789</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <h4>Email:</h4>
                                        <p><a href="mailto:email@domain.com">email@domain.com</a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget widget_nav_menu">
                            <h3 class="widget-title">
                                <span>information</span>
                            </h3>
                            <div class="menu-infomation-container">
                                <ul class="menu">
                                    <li><a href="<?php echo SITE_URL . 'about-us.php'; ?>">About Us</a></li>
                                    <li><a href="<?php echo SITE_URL . 'contact-us.php'; ?>">Contact Us</a></li>
                                    <li><a href="<?php echo SITE_URL . ''; ?>">Term &#038; Conditions</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget widget_nav_menu">
                            <h3 class="widget-title">
                                <span>Customer Care</span>
                            </h3>
                            <div class="menu-customer-care-container">
                                <ul class="menu">
                                    <li><a href="javascript:;">Support</a></li>
                                    <li><a href="javascript:;">Sitemap</a></li>
                                    <li><a href="javascript:;">FAQ</a></li>
                                    <li><a href="javascript:;">Shipping</a></li>
                                    <li><a href="javascript:;">Returns</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget widget_text">
                            <h3 class="widget-title">
                                <span>open house</span>
                            </h3>
                            <div class="textwidget">
                                <ul class="open-time">
                                    <li><span>Mon - Fri:</span><span>8am - 5pm</span> </li>
                                    <li><span>Sat:</span><span>8am - 11am</span> </li>
                                    <li><span>Sun: </span><span>Closed</span></li>
                                </ul>
                                <h3 class="widget-title">
                                    <span>payment Menthod</span>
                                </h3>
                                <div class="payment">
                                    <a href="#"><i class="fa fa-cc-mastercard"></i></a>
                                    <a href="#"><i class="fa fa-cc-visa"></i></a>
                                    <a href="#"><i class="fa fa-cc-paypal"></i></a>
                                    <a href="#"><i class="fa fa-cc-discover"></i></a>
                                    <a href="#"><i class="fa fa-credit-card"></i></a>
                                    <a href="#"><i class="fa fa-cc-amex"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center">
        © 2017 My Mobile Site
    </div>
</footer>
</div>

<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/easing.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/imagesloaded.pkgd.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/bootstrap.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/superfish-1.7.4.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.appear.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/script.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/swatches-and-photos.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.cookie.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.prettyPhoto.init.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.selectBox.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.touchSwipe.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.transit.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.carouFredSel.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/jquery.magnific-popup.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/isotope.pkgd.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.video.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.slideanims.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.actions.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.layeranimation.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.kenburn.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.navigation.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.migration.min.js'></script>
<script type='text/javascript' src='<?php echo SITE_URL; ?>assets/frontend/js/extensions/revolution.extension.parallax.min.js'></script>
<script type="text/javascript">

    var tpj = jQuery;

    var revapi7;
    tpj(document).ready(function () {
        if (tpj("#rev_slider").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider");
        } else {
            revapi7 = tpj("#rev_slider").show().revolution({
                sliderType: "standard",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 50,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    }
                    ,
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        style: "hephaistos",
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: ''
                    }
                },
                gridwidth: 1170,
                gridheight: 600,
                lazyType: "smart",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    });	/*ready*/

    var productArray = [];
    function addtocompare(productID) {
        var found = jQuery.inArray(productID, productArray);
        if (found >= 0) {
            productArray.splice(found, 1);
        } else {
            productArray.push(productID);
        }

        if (productArray.length > 4) {
            alert("please select only 4 phones for comparing");
        }
        if (productArray.length > 0) {
            jQuery(".comparing").find(".count").text(productArray.length);
        } else {
            jQuery(".comparing").find(".count").text('');
        }
        var myJsonString = JSON.stringify(productArray);
        jQuery.cookie("comparing_products", myJsonString);
    }
</script>
</body>
</html>
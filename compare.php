<?php
include_once 'config.php';
include_once 'classes/core.php';
include_once 'includes/frontend_common/header.php';
?>
<div class="heading-container">
    <div class="container heading-standar">
        <div class="page-breadcrumb">
            <ul class="breadcrumb">
                <li>
                    <span>
                        <a class="home" href="#">
                            <span>Home</span>
                        </a>
                    </span>
                </li>
                <li>
                    <span>Compare Phones</span>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="content-container no-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content">
                    <div class="commerce">
                        <div class="col-md-12">
                            <a href="javascript:;" onclick="removeComparing()" class="btn btn-default pull-right">Remove Comparing</a>
                            <?php
                            if (!empty($_COOKIE["comparing_products"])) {
                                $jsonDecodeArray = json_decode($_COOKIE["comparing_products"]);
                                if (!empty($jsonDecodeArray)) {
                                    $countCookie = 1;
                                    foreach ($jsonDecodeArray as $productId) {
                                        echo '<div class="col-md-3">';
                                        if ($countCookie > 4) {
                                            return false;
                                        }

                                        $comparingPhones = $user_obj->getPhonesForCompare($productId);
                                        ?>
                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($productId["phone_id"]) ?>" class="add_to_cart_button">
                                            <img src="<?php echo SITE_URL . "uploads/" . $comparingPhones["phone_image"]; ?>" alt="<?php echo $comparingPhones["phone_name"]; ?>"/>
                                        </a>
                                        <a href="<?php echo SITE_URL . "phone.php?pid=" . base64_encode($productId["phone_id"]) ?>" class="add_to_cart_button">
                                            <h3><?php echo $comparingPhones["phone_name"]; ?></h3>
                                        </a>
                                        <div class="content">
                                            <p><b>Camera :</b> <?php echo $comparingPhones["phone_camera"]; ?></p>
                                            <p><b>Ram :</b> <?php echo $comparingPhones["phone_ram"]; ?></p>
                                            <p><b>Battery :</b> <?php echo $comparingPhones["phone_battery"]; ?></p>
                                            <p><b>Technology :</b> <?php echo $comparingPhones["phone_technology"]; ?></p>
                                            <p><b>Announced :</b> <?php echo $comparingPhones["phone_announced"]; ?></p>
                                            <p><b>Status :</b> <?php echo $comparingPhones["phone_status"]; ?></p>
                                            <p><b>Dimensions :</b> <?php echo $comparingPhones["phone_dimensions"]; ?></p>
                                            <p><b>Weight :</b> <?php echo $comparingPhones["phone_weight"]; ?></p>
                                            <p><b>Sim :</b> <?php echo $comparingPhones["phone_sim"]; ?></p>
                                            <p><b>Type :</b> <?php echo $comparingPhones["phone_type"]; ?></p>
                                            <p><b>Operating System :</b> <?php echo $comparingPhones["phone_os"]; ?></p>
                                            <p><b>Chipset :</b> <?php echo $comparingPhones["phone_chipset"]; ?></p>
                                            <p><b>CPU :</b> <?php echo $comparingPhones["phone_cpu"]; ?></p>
                                            <p><b>GPU :</b> <?php echo $comparingPhones["phone_gpu"]; ?></p>
                                            <p><b>Messaging :</b> <?php echo $comparingPhones["phone_messaging"]; ?></p>
                                        </div>
                                        <?php
                                        echo '</div>';
                                        $countCookie++;
                                    }
                                }
                            } else {
                                ?>
                            <div class="col-md-12" style="text-align: center;">
                                    <p>No comparing are available. Please set phones for comparing!</p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once './includes/frontend_common/footer.php'; ?>
<script>
    function removeComparing() {
        jQuery.removeCookie('comparing_products');
        window.location.reload(true);
    }
</script>
